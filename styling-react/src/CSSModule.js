import React from 'react';
import styles from './CSSModule.module.css'
import ClassNames from 'classnames/bind';

ClassNames('one', 'two');
ClassNames('one', {two:true});
ClassNames('one', {two:false});
ClassNames('one', ['two', 'three']);

const myClass = 'hello';
ClassNames('one', myClass, {myCondition:true});

const cx = ClassNames.bind(styles);


const CSSModule = ({highlighted, theme}) => {
    return (
        <div className={cx('wrapper', 'inverted')}>
        {/*<div className={[styles.wrapper, styles.inverted].join(' ')}>*/}
        {/*<div className={`${styles.wrapper} ${styles.inverted}`}>*/}
            {/*<div className={styles.wrapper}>*/}
            안녕하세요. 저는 <span className="something">CSS Module!</span>

            <div className={ClassNames('myComponent', {highlighted}, theme)}>Hello</div>

        </div>
    );
};

export default CSSModule;