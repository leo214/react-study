import React, {useState, useMemo, useCallback, useRef} from 'react';

const getAverage = numbers => {
    console.log('평균값 계산 중');
    if (numbers.length === 0) return 0;
    const sum = numbers.reduce((a, b) => a + b);
    return sum / numbers.length;
};


const Average = () => {

    const [number, setNumber] = useState('');
    const [list, setList] = useState([]);

    // useRef는 함수형 컴포넌트에서 ref를 쉽게 사용할 수 있도록 해줌
    const inputEl = useRef(null);

    // const onChange = e => {
    //     setNumber(e.target.value);
    // };
    // const onClick = e => {
    //     const nextList = list.concat(parseInt(number));
    //     setList(nextList);
    //     setNumber('');
    // };

    // 렌더링 시 한번만 함수 생성
    const onChange = useCallback(e => {
        setNumber(e.target.value);
    }, []);
    // number 나 list 가 바뀌었을 시에만 함수 생성
    const onClick = useCallback(e => {
        const nextList = list.concat(parseInt(number));
        setList(nextList);
        setNumber('');
        inputEl.current.focus();
    },[number, list]);

    // 특정 값이 바뀌었을 때만 연산 실행. 원하는 값이 바뀌지 않았을 시 이전 연산 결과 그대로 사용
    const avg = useMemo(() => getAverage(list), [list]);

    // 아래 둘다 같은 코드
    // 일반적으로 함수를 재사용할 떄 사용
    useCallback(() => {
        console.log('hello world!');
    }, []);
    // 일반적으로 숫자, 문자열, 객체처럼 일반 값을 재사용할 떄 사용
    useMemo(() => {
        const fn = () => {
            console.log('hello world!');
        };
        return fn;
    });

    return (
        <div>
            <input value={number} onChange={onChange} ref={inputEl}/>
            <button onClick={onClick}>등록</button>
            <ul>
                {list.map((value, index) => (
                    <li key={index}>{value}</li>
                ))}
            </ul>
            <div>
                <b>평균값:</b> {avg}
            </div>
        </div>
    );
};

export default Average;