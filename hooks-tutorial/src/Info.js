import React, {useState, useEffect} from 'react';

const Info = () => {

    const [name, setName] = useState('');
    const [nickname, setNickname] = useState('');

    // 컴포넌트가 렌드링 될 때마다 (class 컴포넌트에서의 componentDidMount + componentDidUpdate 합친 개념)
    // useEffect(() => {
    //     console.log('렌드링이 완료되었습니다!');
    //     console.log({
    //         name,
    //         nickname
    //     });
    // });

    // 컴포넌트가 처음 렌더링 될 때만 실행되고 업데이트시에는 실행하지 않으려면 함수의 두번째 파라미터로 비어있는 배열 넣기
    // useEffect(() => {
    //     console.log('마운트 될 때만 실행됩니다.');
    // }, []);

    // 두번째 파라미터 배열 안에 검사하고 싶은 값을 넣으면 해당 값이 변경될 때만 작동
    // 배열 안에는 useState를 통해 관리하고 있는 상태를 넣어도 되고, props로 전달받은 값을 넣어도 됨
    // useEffect(() => {
    //     console.log(name);
    // }, [name]);

    useEffect(() => {
        console.log('effect'); // 부모 컴포넌트에서 보이기 버튼 클릭 시 노출
        console.log(name);
        // 컴포넌트가 언마운트 되기 전이나 업데이트 직전 작업을 수행하고 싶다면 뒷정리함수를 반환해야 함
        return () => {
            console.log('cleanup'); // 부모 컴포넌트에서 숨기기 버튼 클릭 시 노출
            console.log(name);
        }
    }, [name]);

    const onChangeName = (e) => {
        setName(e.target.value)
    };
    const onChangeNickname = e => {
        setNickname(e.target.value)
    };

    return (
        <div>
            <input type="text" name="username" placeholder="이름" onChange={onChangeName}/>
            <input type="text" name="nickname" placeholder="nickname" onChange={onChangeNickname}/>
            <p>이름 : {name}</p>
            <p>닉네임 : {nickname}</p>
        </div>
    );
};

export default Info;