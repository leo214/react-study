import React, {useReducer} from 'react';

function reducer(state, action){
    switch (action.type) {
        case 'plus':
            return {value : state.value + 1};
        case 'minus':
            return {value : state.value - 1};
        default :
            return state;
    }
}


const Counter2 = () => {

    // useReducer를 사용했을 때 가장 큰 장점은 컴포넌트 업데이트 로직을 컴포넌트 바깥으로 빼낼 수 있음
    const [state, dispatch] = useReducer(reducer, {value : 0});

    return (
        <div>
            <p>현재 카운터 값은 <b>{state.value}</b>입니다.</p>
            <button onClick={() => {dispatch({type : 'plus'})}}>+1</button>
            <button onClick={() => {dispatch({type : 'minus'})}}>-1</button>
            <button onClick={() => {dispatch({type : 'hello'})}}>?</button>
        </div>
    );
};

export default Counter2;