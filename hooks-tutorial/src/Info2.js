import React, {useReducer} from 'react';
import useInputs from './useInputs';

// function reducer(state, action){
//     return {
//         ...state,
//         [action.name] : action.value
//     }
// }

const Info2 = () => {

    const [state, onChange] = useInputs({
        name : '',
        nickname : ''
    });

    // const [state, dispatch] = useReducer(reducer, {
    //     name : '',
    //     nickname : ''
    // });

    // const onChange = (e) => {
    //     dispatch(e.target)
    // };

    const {name, nickname} = state;

    return (
        <div>
            <input name="name" value={name} onChange={onChange}/>
            <input name="nickname" value={nickname} onChange={onChange}/>
            <p>이름 : {name}</p>
            <p>닉네임 : {nickname}</p>
        </div>
    );
};

export default Info2;