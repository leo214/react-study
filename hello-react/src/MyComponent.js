import React, {Component} from 'react';
import PropsTypes from 'prop-types';

class MyComponent extends Component {
    static defaultProps = {
        name : '박소영',
    };
    static propsTypes = {
        name : PropsTypes.string,
        number : PropsTypes.number.isRequired
    };
    render() {
        const {name, children, favoriteNumber} = this.props;
        return (
            <div>
                안녕하세요. 제 이름은 {name} 이고,<br/>
                children 값은 {children}입니다.<br/>
                제가 가장 좋아하는 숫자는 {favoriteNumber}입니다.
            </div>
        );
    }
}
//
// MyComponent.defaultProps = {
//     name : '기본이름'
// };
// MyComponent.propsTypes = {
//     name : PropsTypes.string,
//     favoriteNumber : PropsTypes.number.isRequired
// };

export default MyComponent;