import React ,{useState} from 'react';

const EventPracticeFc = () => {

    // const [username, setUsername] = useState('');
    // const [message, setMessage] = useState('');
    // const onChangeUsername = e => setUsername(e.target.value);
    // const onChangeMessage = e => setMessage(e.target.value);
    // const onClick = () => {
    //     console.log(username + ': ' + message);
    //     setUsername("");
    //     setMessage("");
    // };

    const [form, setForm] = useState({
        username : '',
        message: ''
    });

    const {username, message} = form;

    const onChange = e => {
        const nextForm = {
            ...form,
            [e.target.name]: e.target.value
        };
        setForm(nextForm);
    };
    const onClick = () => {
        console.log(username + ': ' + message);
        setForm({
            username : '',
            message : ''
        })
    };


    const onKeyPress = e => {
        if(e.key === 'Enter'){
            onClick();
        }
    };

    return (
        <div>
            <h1>이벤트 연습</h1>
            <input type="text" name="username" value={username} placeholder="username" onChange={onChange}/>
            <input type="text" name="message" value={message} placeholder="아무거나 입력하세요" onChange={onChange} onKeyPress={onKeyPress}/>
            <button onClick={onClick}>확인</button>
        </div>
    );
};

export default EventPracticeFc;