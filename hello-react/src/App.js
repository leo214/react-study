import React, {Component, Fragment} from 'react';
import './App.css'
import MyComponent from './MyComponent';
import Counter from './Counter';
import Say from './Say';
import EventPractice from './EventPractice';
import EventPracticeFc from './EventPracticeFc';
import ValidationSample from './validatonSample'
import CreateRef from './createRef'
import ScrollBox from './ScrollBox'
import InterationSample from './IterationSample'
import LifeCycleSample from './LifeCycleSample'
import ErrorBoundary from "./ErrorBoundary";

function getRandomColor(){
    return '#' + Math.floor(Math.random() * 1677215).toString(16);
}

class App extends Component {

    state = {
        color: '#000000'
    };

    handleClick = () => {
        this.setState({
            color:getRandomColor()
        });
    };

    render() {
        const name = '리액트';
        return (
            <Fragment>
                {/*<h1 className="react">{name}</h1>*/}
                {/*<MyComponent ref={(ref) => this.myComponent = ref} favoriteNumber={2}>리액트</MyComponent>*/}
                <Counter/>
                {/*<Say/>*/}
                {/*<EventPractice/>*/}
                {/*<EventPracticeFc/>*/}
                {/*<ValidationSample/>*/}
                {/*<CreateRef/>*/}
                {/*<ScrollBox ref={(ref) => this.scrollBox = ref}/>*/}
                {/*<button onClick={() => {this.scrollBox.scrollToBottom()}}>맨 밑으로</button>*/}
                {/*<InterationSample/>*/}
                {/*<button onClick={this.handleClick}>랜덤색상</button>*/}
                {/*<ErrorBoundary>*/}
                    {/*<LifeCycleSample color={this.state.color}/>*/}
                {/*</ErrorBoundary>*/}
            </Fragment>
        );
    }
}

export default App;