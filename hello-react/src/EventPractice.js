import React, {Component} from 'react';

class EventPractice extends Component {

    state = {
        username : '',
        message : ''
    };

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    handleClick = (e) => {
        console.log(this.state.username + ": " + this.state.message);
        this.setState({
            username : '',
            message : ''
        })
    };
    handleKeyPress = (e) => {
        if(e.key === 'Enter'){
            this.handleClick();
        }
    }

    // constructor(props){
    //     super(props);
    //     this.handleChange = this.handleChange.bind(this);
    //     this.handleClick = this.handleClick.bind(this);
    // }
    // handleChange(e){
    //     this.setState({message:e.target.value});
    // }
    // handleClick(e){
    //     console.log(this.state.message);
    //     this.setState({message:''});
    // }

    render() {
        return (
            <div>
                <h1>이벤트 연습</h1>
                <input type="text" name="username" value={this.state.username} placeholder="username" onChange={this.handleChange}/>
                <input type="text" name="message" value={this.state.message} placeholder="아무거나 입력하세요" onChange={this.handleChange} onKeyPress={this.handleKeyPress}/>
                <button onClick={this.handleClick}>확인</button>
            </div>
        );
    }
}

export default EventPractice;