import React, {useState} from 'react';

const Say = () => {
    const [message, setMessage] = useState('');
    const onClickEnter = () => setMessage('안녕하세요.');
    const onClickLeave = () => setMessage('안녕히가세요.');
    const [color,setColor] = useState('gray');

    return (
        <div>
            <button onClick={onClickEnter}>입장</button>
            <button onClick={onClickLeave}>퇴장</button>
            <h1 style={{color}}>{message}</h1>
            <button style={{color:"skyblue"}} onClick={() => setColor('skyblue')}>skyblue</button>
            <button style={{color:"pink"}} onClick={() => setColor('pink')}>pink</button>
            <button style={{color:"yellowgreen"}} onClick={() => setColor('yellowgreen')}>yellowgreen</button>
        </div>
    );
};

export default Say;