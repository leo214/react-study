import React, {Component} from 'react';

class LifeCycleSample extends Component {
    state = {
        number: 0,
        color: null
    };

    myRef = null;

    // 마운트 - 클래스 생성자 메서드
    /* 컴포넌트를 만들 때 처음으로 실행됨. 초기 state 정함 */
    constructor(props) {
        super(props);
        console.log('constructor');
    }

    // 마운트 - props 값을 state에 넣을 때 사용되는 메서드드
    // 업데이트 - props 변화에 따라 state 값에도 변화를 주고 싶을 때
    static getDerivedStateFromProps(nextProps, prevState) {
        console.log('getDerivedStateFromProps');
        if (nextProps.color !== prevState.color) {
            return { color: nextProps.color };
        }
        return null;
    }
    /* props로 받아온 값을 state에 동기화 시키는 용도로 사용. */

    // 마운트 - 컴포넌트가 웹 브라우저상에 나타난 후 호출하는 메서드
    /* 컴포넌트를 만들고 첫 렌더링 다 마친 후 실행.
     * 다른 자바스크립트 라이브러리 또는 프레임워크의 함수 호출. 이벤트 등록 같은 비동기 작업 처리 */
    componentDidMount() {
        console.log('componentDidMount');
    }

    // 업데이트 - 컴포넌트가 리렌더링을 해야할지 말아야 할지 결정하는 메서드
    /* props 또는 state를 변경했을 떄 리렌더링을 시작할지 여부 지정하는 메서드
     * true 또는 false 값을 반드시 반환.
      * 컴포넌트 생성시 따로 메서드를 생성하지 않으면 기본적으로 true 값 반환
      * false 값을 반환시에 업데이트 과정은 여기서 중지
      * 현재 props, state = this.props/this.state
      * 새로 설정될 props, state = nextProps, nextState*/
    shouldComponentUpdate(nextProps, nextState) {
        console.log('shouldComponentUpdate', nextProps, nextState);
        // 숫자의 마지막 자리가 4면 리렌더링하지 않습니다.
        return nextState.number % 10 !== 4;
    }

    // 언마운트 - 컴포넌트가 웹 브라우저상에서 사라지기 전 호출하는 메서드
    /* 컴포넌트를 DOM에서 제거할 떄 실행.componentDidMount 에서 등록한 이벤트,타이머, 직접생성한 DOM이 있을시 여기서 제거작업해야함 */
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }

    handleClick = () => {
        this.setState({
            number: this.state.number + 1
        });
    };

    // 업데이트 - 컴포넌트 변호를 dom에 반영하기 직전 호출하는 메서드
    /* render에서 만들어진 결과물이 브라우저에 실제로 반영되기 직전에 호출.
     * 메서드에서 반환하는 값은 세번째 componentDidUpdate 세번째 파라미터인 snapshot값으로 전달받을 수 있음
      * 주로 업데이트 직전 값을 참고할 일이 있을 떄 활용*/
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('getSnapshotBeforeUpdate');
        if (prevProps.color !== this.props.color) {
            return this.myRef.style.color;
        }
        return null;
    }

    // 업데이트 - 컴포넌트 업데이트 작업이 끝난 후 호출하는 메서드
    /* 리렌더링 완료 후 실행. 업데이트 끝난 직후이므로 dom 관련 처리를 해도 무방.
     * orevOrios / prevState를 사용하여 컴포넌트가 이전에 가졌던 데이터에 접근
      * getSnapshotBeforeUpdate에서 반환한 값이 있으면 여기서 snapshot 값을 전달 받을 수 있음*/
    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate', prevProps, prevState);
        if (snapshot) {
            console.log('업데이트되기 직전 색상: ', snapshot);
        }
    }

    // 마운트/업데이트 - UI 렌드링하는 메서드
    /* 컴포너느 모양새 정의
     * 라이프사이클 메서드 중 유일한 필수 메서드*/
    render() {
        console.log('render');
        const style = {
            color: this.props.color
        };
        return (
            <div>
                 {this.props.missing.value}
                <h1 style={style} ref={ref => (this.myRef = ref)}>
                    {this.state.number}
                </h1>
                <p>color: {this.state.color}</p>
                <button onClick={this.handleClick}>더하기</button>
            </div>
        );
    }

    /* ComponentDidCatch 메서드 : 렌더링 도중 에러가 발생했을 때 먹통이 되지 않고 오류 UI를 보여줄 수 있게 해줌.
     * error는 어떤 에러가 발생했는지. info는 어디에 있는 코드에서 오류 발생했는지 알려줌.
      * 하지만 컴포넌트 자신에게 발생하는 에러를 잡아낼 수 없고, 자신의 this.props.child로 전달되는 컴포넌트에서 발생하는 에러만 잡아낼 수 있음*/
    // componentDidCatch(error, info){
    //     this.setState({
    //         error:true
    //     });
    //     console.log({error, info});
    // }
}

export default LifeCycleSample;