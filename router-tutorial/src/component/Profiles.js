import React from 'react';
import {NavLink, Route, Link} from 'react-router-dom';
import Profile from './Profile';

const Profiles = () => {

    const activityStyle = {
        background:'black',
        color:'white'
    };

    return (
        <div>
            <h3>사용자 목록 :</h3>
            <ul>
                <li>
                    {/*<Link to='/profiles/velopert'>velopert</Link>*/}
                    <NavLink activeStyle={activityStyle} to='/profiles/velopert'>velopert</NavLink>
                </li>
                <li>
                    {/*<Link to='/profiles/leo'>leo</Link>*/}
                    <NavLink activeStyle={activityStyle} to='/profiles/leo'>leo</NavLink>
                </li>
            </ul>

            <Route path="/profiles" exact render={() => <div>사용자를 선택해주세요</div>}/>
            <Route path="/profiles/:username" component={Profile}/>


        </div>
    );
};

export default Profiles;