import React from 'react';
import {withRouter} from 'react-router-dom';
import WithRouterSample from "./WithRouterSample";

const data = {
    velopert : {
        name : '김민준',
        description : '리액트를 좋아하는 개발자'
    },
    leo : {
        name : '박소영',
        description : '열심히 성장하는 똘똘한 개발자'
    }
};

const Profile = ({ match }) => {
    const { username } = match.params;
    const profile = data[username];
    if (!profile) {
        return <div>존재하지 않는 사용자입니다.</div>;
    }
    return (
        <div>
            <h3>
                {username}({profile.name})
            </h3>
            <p>{profile.description}</p>
            <WithRouterSample/>
        </div>
    );
};

export default Profile;